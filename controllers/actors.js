const express = require('express');
const { Actor } = require('../db');

function create(req, res, next) {
    let name = req.body.name;
    let lastName = req.body.lastName;
    
    let actor = new Object({
        name: name,
        lastName: lastName,
    });

    Actor.create(actor)
        .then(actor => res.json(actor))
        .catch(err => res.send(err));

}

function list(req, res, next) {
    let page = req.params.page ? req.params.page : 0;
    Actor.findAll({ include:['movies']})
        .then(objects => res.json(objects));

function index(req, res, next) {
    const id = req.params.id;
    Actor.findByPk(id).then(object => res.json(object));
}

function edit(req, res, next) {
    const id = req.params.id;
    Actor.findByPk(id).then(object => {
        object.name = req.body.name ? req.body.name : object.name;
        object.lastName = req.body.lastName ? req.body.lastName : object.lastName;

        object.update({'name': object.name, 'lastName': object.lastName})
            .then(object => res.json(object));
    });
}

function replace(req, res, next) {
    const id = req.params.id;
    Actor.findByPk(id).then(object => {
        object.update({'name': req.body.name, 'lastName': req.body.lastName})
            .then(object => res.json(object));
    });
}

function destroy(req, res, next) {
    const id = req.params.id;
    Actor.destroy({where:{id:id}}).then(object => res.json(object));
}
module.exports = {
    create, list, index, edit, replace, destroy
}
