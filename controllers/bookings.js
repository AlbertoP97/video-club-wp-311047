const express = require('express');
const { Booking } = require('../db');

function create(req, res, next) {
    let date = req.body.date;
    let memberId = req.body.memberId;
    let copyId = req.body.copyId;
    
    let booking = new Object({
        date: date,
        memberId: memberId,
        copyId: copyId
    });

    Booking.create(booking)
        .then(booking => res.json(booking))
        .catch(err => res.send(err));

}

function list(req, res, next) {
    let page = req.params.page ? req.params.page : 0;
    Booking.findAll({include:['movie', 'copy']})
        .then(objects => res.json(objects));
}

function index(req, res, next) {
    const id = req.params.id;
    Booking.findByPk(id).then(object => res.json(object));
}

function edit(req, res, next) {
    const id = req.params.id;
    Booking.findByPk(id).then(object => {
        object.date = req.body.date ? req.body.date : object.date;
        object.memberId = req.body.memberId ? req.body.memberId : object.memberId;
        object.copyId = req.body.copyId ? req.body.copyId : object.copyId;

        object.update({'date': object.date, 'memberId': object.memberId, 'copyId': object.copyId})
            .then(object => res.json(object));
    });
}

function replace(req, res, next) {
    const id = req.params.id;
    Booking.findByPk(id).then(object => {
        object.update({'date': req.body.date, 'memberId': req.body.memberId, 'copyId': req.body.copyId})
            .then(object => res.json(object));
    });
}

function destroy(req, res, next) {
    const id = req.params.id;
    Booking.destroy({where:{id:id}}).then(object => res.json(object));
}
module.exports = {
    create, list, index, edit, replace, destroy
}