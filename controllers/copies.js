const express = require('express');
const { Copy } = require('../db');

function create(req, res, next) {
    let number = req.body.number;
    let format = req.body.format;
    let movieId = req.body.movieId;
    let status = req.body.status;
    
    let copy = new Object({
        number: number,
        format: format,
        movieId: movieId,
        status: status
    });

    Copy.create(copy)
        .then(copy => res.json(copy))
        .catch(err => res.send(err));

}

function list(req, res, next) {
    let page = req.params.page ? req.params.page : 0;
    Copy.findAll({include:['movie']})
        .then(objects => res.json(objects));
}

function index(req, res, next) {
    const id = req.params.id;
    Copy.findByPk(id).then(object => res.json(object));
}

function edit(req, res, next) {
    const id = req.params.id;
    Copy.findByPk(id).then(object => {
        object.number = req.body.number ? req.body.number : object.number;
        object.format = req.body.format ? req.body.format : object.format;
        object.movieId = req.body.movieId ? req.body.movieId : object.movieId;
        object.status = req.body.status ? req.body.status : object.status;

        object.update({'number': object.number, 'format': object.format, 'movieId': object.movieId, 
            'status': object.status}).then(object => res.json(object));
    });
}

function replace(req, res, next) {
    const id = req.params.id;
    Copy.findByPk(id).then(object => {
        object.update({'number': req.body.number, 'format': req.body.format, 'movieId': req.body.movieId,
            'status': req.body.status}).then(object => res.json(object));
    });
}

function destroy(req, res, next) {
    const id = req.params.id;
    Copy.destroy({where:{id:id}}).then(object => res.json(object));
}
module.exports = {
    create, list, index, edit, replace, destroy
}