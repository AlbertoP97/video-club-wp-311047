const express = require('express');
const { Director } = require('../db');

function create(req, res, next) {
    let name = req.body.name;
    let lastName = req.body.lastName;
    
    let director = new Object({
        name: name,
        lastName: lastName,
    });

    Director.create(director)
        .then(director => res.json(director))
        .catch(err => res.send(err));

}

function list(req, res, next) {
    let page = req.params.page ? req.params.page : 0;
    Director.findAll().then(objects => res.json(objects));
}

function index(req, res, next) {
    const id = req.params.id;
    Director.findByPk(id).then(object => res.json(object));
}

function edit(req, res, next) {
    const id = req.params.id;
    Director.findByPk(id).then(object => {
        object.name = req.body.name ? req.body.name : object.name;
        object.lastName = req.body.lastName ? req.body.lastName : object.lastName;

        object.update({'name': object.name, 'lastName': object.lastName})
            .then(object => res.json(object));
    });
}

function replace(req, res, next) {
    const id = req.params.id;
    Director.findByPk(id).then(object => {
        object.update({'name': req.body.name, 'lastName': req.body.lastName})
            .then(object => res.json(object));
    });
}

function destroy(req, res, next) {
    const id = req.params.id;
    Director.destroy({where:{id:id}}).then(object => res.json(object));
}
module.exports = {
    create, list, index, edit, replace, destroy
}
