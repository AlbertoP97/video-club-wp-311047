const express = require('express');
const { Member } = require('../db');

function create(req, res, next) {
    let name = req.body.name;
    let lastName = req.body.lastName;
    let address = req.body.lastName;
    let phone = req.body.phone;
    let status = req.body.status;
    
    let Member = new Object({
        name: name,
        lastName: lastName,
        address: address,
        phone: phone,
        status: status,
    });

    Member.create(member)
        .then(member => res.json(member))
        .catch(err => res.send(err));

}

function list(req, res, next) {
    let page = req.params.page ? req.params.page : 0;
    Member.findAll().then(objects => res.json(objects));
}

function index(req, res, next) {
    const id = req.params.id;
    Member.findByPk(id).then(object => res.json(object));
   
}

function edit(req, res, next) {
    const id = req.params.id;
    Member.findByPk(id).then(object => {
        object.name = req.body.name ? req.body.name : object.name;
        object.lastName = req.body.lastName ? req.body.lastName : object.lastName;
        object.address = req.body.address ? req.body.address : object.address;
        object.phone = req.body.phone ? req.body.phone : object.phone;
        object.status = req.body.status ? req.body.status : object.status;

        object.update({
            'name': object.name, 'lastName': object.lastName, 
            'address': object.address, 'phone': object.phone, 
            'status': object.status}).then(object => res.json(object));
    });
   
}

function replace(req, res, next) {
    const id = req.params.id;
    Member.findByPk(id).then(object => {
        object.update({
            'name': req.body.name, 'lastName': req.body.lastName, 
            'address': req.body.address, 'phone': req.body.phone, 
            'status': req.body.status}).then(object => res.json(object));
    }); 
}

function destroy(req, res, next) {
    const id = req.params.id;
    Member.destroy({where:{id:id}}).then(object => res.json(object));
}
module.exports = {
    create, list, index, edit, replace, destroy
}
