const express = require('express');
const { Movie } = require('../db');
const { Actor } = require('../db');

function create(req, res, next) {
    let title = req.body.title;
    let genreId = req.body.genreId;
    let directorId = req.body.directorId;
    
    let movie = new Object({
        title: title,
        genreId: genreId,
        directorId: directorId
    });

    Movie.create(movie)
        .then(movie => res.json(movie))
        .catch(err => res.send(err));

}

function addActor(req, res, next){
    let id = req.body.id;
    let actorId = req.body.actorId;

    Movie.findByPk(id).then((movie)=>{
        Actor.findByPk(actorId).then((actor) => {
            movie.addActor(actor);
            res.json(movie);
        });
    });
}

function list(req, res, next) {
    let page = req.params.page ? req.params.page : 0;
    Movie.findAll({include:['genre', 'director', 'actors']})
        .then(objects => res.json(objects));
}

function index(req, res, next) {
    const id = req.params.id;
    Movie.findByPk(id).then(object => res.json(object));
}

function edit(req, res, next) {
    const id = req.params.id;
    Movie.findByPk(id).then(object => {
        object.title = req.body.title ? req.body.title : object.title;
        object.genreId = req.body.genreId ? req.body.genreId : object.genreId;
        object.directorId = req.body.directorId ? req.body.directorId : object.directorId;

        object.update({'title': object.title, 'genreId': object.genreId, 'directorId': object.directorId})
            .then(object => res.json(object));
    });
}

function replace(req, res, next) {
    const id = req.params.id;
    Movie.findByPk(id).then(object => {
        object.update({'title': req.body.title, 'genreId': req.body.genreId, 'directorId': req.body.directorId})
            .then(object => res.json(object));
    });
}

function destroy(req, res, next) {
    const id = req.params.id;
    Movie.destroy({where:{id:id}}).then(object => res.json(object));
}
module.exports = {
    create, addActor, list, index, edit, replace, destroy
}
