const express = require('express');

function create(req, res, next) {
    let user = new Object();

    
    res.json(`Genera un nuevo usuario del sistema ${user}`);
}

function list(req, res, next) {
    res.send('Mostrar todos los usuarios del sistema');
}

function index(req, res, next) {
    const id = req.params.id;
    res.send(`Mostrar el usuario del sistema con id = ${id}`);
}

function edit(req, res, next) {
    const id = req.params.id;
    res.send(`Modificar el usuario del sistema con id = ${id}`);
}

function replace(req, res, next) {
    const id = req.params.id;
    res.send(`Reemplazar el usuario del sistema con id = ${id}`);
}

function destroy(req, res, next) {
    const id = req.params.id;
    res.send(`Eliminar el usuario del sistema con id = ${id}`);
}
module.exports = {
    create, list, index, edit, replace, destroy
}
