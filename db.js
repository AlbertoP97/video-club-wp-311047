const Sequelize = require('sequelize');

const directorModel = require('./models/director');
const genreModel = require('./models/genre');
const actorModel = require('./models/actor');
const memberModel = require('./models/member');
const movieModel = require('./models/movie');
const movieActorModel = require('./models/movieactor');
const copyModel = require('./models/copy');
const bookingModel = require('./models/booking');

// 1) db name 2) user 3) password 4) Obj configuracion

const sequelize = new Sequelize('video-club', 'root', 'secret', {
    host: 'localhost', // Direccion del RDBMS
    dialect: 'mysql' // Dialecto o lenguaje
});

const Director = directorModel(sequelize, Sequelize);
const Genre = genreModel(sequelize, Sequelize);
const Actor = actorModel(sequelize, Sequelize);
const Member = memberModel(sequelize, Sequelize);
const Movie = movieModel(sequelize, Sequelize);
const MovieActor = movieActorModel(sequelize, Sequelize);
const Copy = copyModel(sequelize, Sequelize);
const Booking = bookingModel(sequelize, Sequelize);

// Un genero puede tener muchas peliculas
Genre.hasMany(Movie, {as: 'movies'});

// Una pelicula puede tener un genero
Movie.belongsTo(Genre, {as: 'genre'});

// Un director puede tener muchas peliculas
Director.hasMany(Movie, {as: 'movies'});

// Una pelicula puede tener un director
Movie.belongsTo(Director, {as: 'director'});

// Un actor participa en muchas peliculas
MovieActor.belongsTo(Movie, {foreignKey: 'movieId'});

// En una pelicula participan muchos actores
MovieActor.belongsTo(Actor, {foreignKey: 'actorId'});

// Una pelicula puede tener muchas copias
Movie.hasMany(Copy, {as: 'copies'});

// Una copia puede tener una pelicula 
Copy.belongsTo(Movie, {as: 'movie'});

// Una copia puede tener muchas rentas
Copy.hasMany(Booking, {as: 'bookings'});

// Una renta puede tener una copia
Booking.belongsTo(Copy, {as: 'copy'});

// Un miembro puede tener muchas rentas
Member.hasMany(Booking, {as: 'bookings'});

// Una renta puede tener a un miembro
Booking.belongsTo(Member, {as: 'member'});


Movie.belongsToMany(Actor, {
    foreignKey: 'actorId',
    as: 'actors',
    through: 'moviesActors'
});

Actor.belongsToMany(Movie, {
    foreignKey: 'movieId',
    as: 'movies',
    through: 'moviesActors'
});

sequelize.sync({
    force: true
}).then(()=>{
    console.log("BD creada correctamente");
});

module.exports = { Director, Genre, Actor, Member, Movie, MovieActor, Copy, Booking };