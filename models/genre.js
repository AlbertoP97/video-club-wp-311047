module.exports = (sequelize, type) =>{
    const Genre = sequelize.define('genre', {
        id : {type: type.INTEGER, primaryKey:true, autoIncrement:true},
        description : {type: type.STRING, notNull:true},
        status : {type: type.BOOLEAN, notNull: true}
    });
    return Genre;
};