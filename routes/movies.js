const express = require('express');
const controller = require('../controllers/movies');
const router = express.Router();

/* GET users listing. */
router.post('/', controller.create);

router.post('/actor', controller.addActor);

router.get('/:page?', controller.list);

router.get('/show/:id', controller.index);

router.put('/:id', controller.edit);

router.patch('/:id', controller.replace);

router.delete('/:id', controller.destroy);

module.exports = router;
